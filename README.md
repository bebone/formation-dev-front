# Formation Front Valentin

## Version :

- 1.0 : Mise en place de la base du dépôt et de la documentation.


## Sommaire :

* Formation git

    * Présentation de git

    * Installation de git

    * Initialiser un dépôt git

    * Envoyer ses modifications

    * Recevoir des modifications

    * Les branches

        * Pourquoi les branches

* Formation HTML 5 / CSS 3

    * Documentation

* Bootstrap

    * Installation de bootstrap

    *




## Formation git

### Présentation de git
git est un logiciel de gestion de versions décentralisé. C'est un logiciel libre créé par Linus Torvalds, auteur du noyau Linux, et distribué selon les termes de la licence publique générale GNU version 2. En 2016, il s’agit du logiciel de gestion de versions le plus populaire qui est utilisé par plus de douze millions de personnes.

Git permet de :

* Versionner un travail

* Travailler à plusieurs sur un même projet

* Beaucoup d'autres choses

[lien vers wikipedia](https://fr.wikipedia.org/wiki/Git).

### Installation de git

Installation de git sur Fedora :
```shell
# Mise à jour du système.
sudo dnf upgrade -y

# Installation de git.
sudo dnf install git

# Installation d'outil graphique de vue des branches
sudo dnf install gitg
```

### Initialiser un dépôt git

Pour créer un dépôt git :
```shell
# Création d'un dossier de travail contenant l'ensemble de ses projets
# Workspace par exemple
mdkir Workspace
cd Workspace

# Création d'un dépôt git local
mkdir $le_nom_de_mon_projet
cd $le_nom_de_mon_projet
git init

# Récuperer un dépot en ligne, 2 méthodes :
## méthode 1
cd Workspace
git clone $url_du_depot

## methode 2
cd Workspace
git remote add origin $url_du_depot
git fetch origin
git pull origin master

```

### Envoyer ses modifications

Faire des modifications dans un fichier et les enregistrer.
```shell
# Afficher la liste des fichiers modifiés
git status

# Afficher les modifications sur un fichier
git diff HEAD $le_nom_du_fichier

# Si les modifications sont OK, ajouter les fichiers
## ajouter un seul fichier
git add $le_nom_du_fichier
## ajouter plusieurs fichiers
git add $le_nom_du_fichier_1 $le_nom_du_fichier_2
## ajouter tout les fichiers
git add --all
## outil de gestion "graphique"
git add -i

# Normalement si tout fonctionne, les fichiers sont passé de la couleur rouge vers la couleur verte

# Expliquer ses modifications
## La version longue
git commit
### Un editeur en terminal s'ouvre, expliquer les modifications et enregistrer

## La version courte
git commit -m'explication de la modification'

# Envoyer les modifications
## Avant d'envoyer les modifications il faut récuperer les modifications des autres
git pull --rebase
## Envoyer ses modifications
git push
```

### Recevoir des modifications
```shell
git fetch origin
# commit l'ensemble de ses modifications
git pull --rebase
```


### Les branches

#### Pourquoi les branches

@TODO

## Formation HTML 5 / CSS 3

Beaucoup de documentation présente sur internet, voici les principaux sites :

[apprendre HTML5 / CSS3](https://openclassrooms.com/courses/apprenez-a-creer-votre-site-web-avec-html5-et-css3)

Structure de base d'une page html

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<header>
            <!-- header content -->
		</header>

		<section>
            <!-- main content-->
		</section>

		<footer>
            <!-- footer content -->
		</footer>
	</body>
</html>
```


## Bootstrap

Documentation utile :

* [Documentation officielle](getbootstrap.com)