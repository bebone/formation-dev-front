# EXERCICE 1

## CONSIGNES

Réalisation d'une page bootstrap de base avec :

* Un header qui ne se trouve pas dans des grilles bootstrap

* 2 sections :

    * une section de 8 colonnes

    * une section de 4 colonnes

